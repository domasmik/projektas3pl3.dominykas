﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal.Enumerations
{
    public enum AccommodationTypeEnum
    {
        [Display(Name = "Bed and Breakfest")]
        BedAndBreakfest = 1,

        [Display(Name = "All Inclusive")]
        AllInclusive = 2,

        [Display(Name = "Full Pansion")]
        FullPansion = 3
    }
}
