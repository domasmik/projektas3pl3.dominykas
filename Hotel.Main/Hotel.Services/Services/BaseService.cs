﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Hotel.Services
{
    public class BaseService : DbConnection
    {
        public BaseService()
        {
        }

        protected void OpenConnecion()
        {
            if (hotelConnection.State == System.Data.ConnectionState.Closed)
            {
                hotelConnection.Open();
            }
        }

        protected void CloseConnection()
        {
            if (hotelConnection.State == System.Data.ConnectionState.Open)
            {
                hotelConnection.Close();
            }
        }
    }
}
