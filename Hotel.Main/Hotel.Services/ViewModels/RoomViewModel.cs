﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dal.Enumerations;
using Hotel.Dal.DataAnnotations;

namespace Hotel.ViewModels
{
    public class RoomViewModel
    {
        [FormProperty(ControlName = "txtRoomId")]
        public int? RoomId { get; set; }

        [FormProperty(ControlName = "txtRoomNumber")]
        public int RoomNumber { get; set; }

        [FormProperty(ControlName = "cmbxRoomType")]
        public RoomTypeEnum RoomType { get; set; }

        [FormProperty(ControlName = "txtAdditionalInfo")]
        public string AdditionalInfo { get; set; }

        [FormProperty(ControlName = "txtCapacity")]
        public int Capacity { get; set; }

        public bool IsRoomNumberAvailable { get; set; }
    }
}
